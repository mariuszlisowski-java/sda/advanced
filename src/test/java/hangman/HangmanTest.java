package hangman;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HangmanTest {
    private Hangman game = new Hangman();

    @Test
    public void maskedPuzzled() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        String output = game.getOutput();

        // then
        assertEquals("... .. ....", output);
    }

    @Test
    public void guessOneLetter() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("m");
        String output = game.getOutput();

        // then
        assertEquals("... m. ....", output);
    }

    @Test
    public void guessOneUppercaseLetter() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("K");
        String output = game.getOutput();

        // then
        assertEquals("... .. k...", output);
    }

    @Test
    public void guessOneUppercaseOneLowercaseLetter() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("A");
        String output = game.getOutput();

        // then
        assertEquals("A.a .a ...a", output);
    }

   @Test
    public void guessTwoLetters() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("ma");
        String output = game.getOutput();

        // then
        assertEquals("A.a ma ...a", output);
    }

   @Test
    public void guessAllLetters() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("Ala ma kota");
        String output = game.getOutput();

        // then
        assertEquals("Ala ma kota", output);
    }

    @Test
    public void guessAllLettersCaseReversed() {
        // given
        game.setPuzzle("Ala ma kota");

        // when
        game.guess("aLA MA KoTA");
        String output = game.getOutput();

        // then
        assertEquals("Ala ma kota", output);
    }


}

    // odkrywamy a -> widoczne A, a
    // odkywac A -> widoczne A, a
    // odkrywamy m,a -> widoczne m,a
    // odkryj cala zagadke -> widoczna cala zagadka
    // odkryj cala zagadke -> widoczna cala zagadka - nie patrzy na wielkosc znakow


// na poaczatku mamy 7 hp
// jesli sie pomylimy tracimy 1 hp
// jesli jest 0 hp nie mozna dalej zgadywac
// 7 hp po ustawieniu nowej zagadki - i set pusty
