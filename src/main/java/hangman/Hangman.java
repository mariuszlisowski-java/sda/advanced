package hangman;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Hangman {
    private String puzzle;
    private Set<String> guessList = new HashSet<>();

    public void setPuzzle(String puzzle) {
        this.puzzle = puzzle;
    }

    public String getOutput() {
        return Stream.of(puzzle.split(""))
                .map(letter -> {
                    if (Character.isWhitespace(letter.charAt(0))) {
                        return " ";
                    } else {
                        if (guessList.contains(letter.toLowerCase())) {
                            return letter;
                        } else {
                            return ".";
                        }
                    }})
                .reduce((element, accumulator) -> element + accumulator).get();
    }

    public void guess(String guess) {
        char[] chars = guess.toCharArray();
        for (char ch : chars) {
            guessList.add(String.valueOf(Character.toLowerCase(ch)));

        }

    }
}
