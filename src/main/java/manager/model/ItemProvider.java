package manager.model;

import java.util.List;

public interface ItemProvider {
    List<Item> getItems();
}
