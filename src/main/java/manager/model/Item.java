package manager.model;

import java.time.LocalDate;

public class Item {
    public final String shop;
    public final LocalDate date;
    public final double price;

    public Item(String shop, LocalDate date, double price) {
        this.shop = shop;
        this.date = date;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "shop='" + shop + '\'' +
                ", date=" + date +
                ", price=" + price +
                '}';
    }
}
