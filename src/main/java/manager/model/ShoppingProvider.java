package manager.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ShoppingProvider implements ItemProvider {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private List<Item> items = new ArrayList<>();

    @Override
    public List<Item> getItems() {
        return items;
    }

    public ShoppingProvider() {
        loadFile();
    }

    public void loadFile() {
        File file = new File("zakupy.csv");

        boolean isHeader = true;
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;
            while((line = bufferedReader.readLine()) != null){
                if (!isHeader) {
                    System.out.println(line);
                    parseAndAdd(line);
                } else {
                   isHeader = false;
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void parseAndAdd(String line) {
        String[] split = line.split(";");
        String shop = split[0];
        double price = Double.parseDouble(split[1]
                .replace(",", ".")
                .replace("\"", ""));
        LocalDate date = LocalDate.parse(split[2], formatter);

        items.add(new Item(shop, date, price));
    }

}
