package manager;

import manager.model.Item;

import java.util.List;

public class MoneyContract {
    public interface View {
        void refreshList(List<Item> items);

        void showSum(double sum);
    }
    public interface Presenter {
        void prepareData();
        void onNameChange(String name);
    }
}
