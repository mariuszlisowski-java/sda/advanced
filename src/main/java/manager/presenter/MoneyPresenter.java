package manager.presenter;

import manager.model.Item;
import manager.model.ItemProvider;
import manager.MoneyContract;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MoneyPresenter implements MoneyContract.Presenter {
    private final MoneyContract.View view;
    private final ItemProvider provider;

    public MoneyPresenter(MoneyContract.View view, ItemProvider provider) {
        this.view = view;
        this.provider = provider;
    }

    @Override
    public void prepareData() {
        List<Item> items = provider.getItems();
        refreshContent(items);
    }

    private void refreshContent(List<Item> items) {
        view.refreshList(items);

        double sum = getPricesSimple(items);
//        Optional<Double> sum = getPrices(items);

        view.showSum(sum);
    }

    @Override
    public void onNameChange(String name) {
        List<Item> output = provider.getItems()
                .stream()
                .filter(item -> item.shop.contains(name))
                .collect(Collectors.toList());
        refreshContent(output);
    }

    private double getPricesSimple(List<Item> items) {
        double sum = 0;
        for (Item item : items) {
            sum += item.price;
        }
        return sum;
    }

    // optional implementation of above (unused)
    private Optional<Double> getPricesReduce(List<Item> items) {
        Optional<Double> sum = items.stream()
                .map(item -> item.price)
                .reduce((identity, acumulator) -> identity + acumulator);
        return sum;
    }

    // optional implementation of above (unused)
    private void getPricesMap(List<Item> items) {
        List<Double> prices = items.stream()
                .map(item -> item.price)
                .collect(Collectors.toList());
        double sum = 0;
        for (double price : prices) {
            sum += price;
        }
    }
}
