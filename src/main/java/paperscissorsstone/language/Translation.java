package paperscissorsstone.language;

import paperscissorsstone.enums.GameAction;
import paperscissorsstone.player.Player;

public interface Translation {

    String actionText1(GameAction action1, Player player1);

    String actionText2(GameAction action2, Player player2);
}
