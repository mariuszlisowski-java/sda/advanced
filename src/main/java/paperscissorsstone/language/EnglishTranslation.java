package paperscissorsstone.language;

import paperscissorsstone.enums.GameAction;
import paperscissorsstone.player.Player;

public class EnglishTranslation implements Translation {
    @Override
    public String actionText1(GameAction action1, Player player1) {
        return String.format("Computer chooses %s", action1);

    }

    @Override
    public String actionText2(GameAction action2, Player player2) {
        return String.format("Human chooses %s", action2);
    }
}
