package paperscissorsstone.enums;

public enum GameAction {
    ROCK, PAPER, SCISSORS
}
