package paperscissorsstone.enums;

public enum GameResult {
    PLAYER_1_WIN("Player 1 wins"),
    PLAYER_2_WIN("Player 2 wins"),
    DRAW("No winners");

    String result;
    GameResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
