package paperscissorsstone.player;


import paperscissorsstone.enums.GameAction;

public interface Player {
    GameAction chooseAction();

    String playerWin();
}
