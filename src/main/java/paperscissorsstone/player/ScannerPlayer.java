package paperscissorsstone.player;

import paperscissorsstone.enums.GameAction;

import java.security.InvalidParameterException;
import java.util.Scanner;

public class ScannerPlayer implements Player {
    @Override
    public GameAction chooseAction() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose action: ");
        System.out.print("1. Rock, 2. Paper, 3. Scissors\n: ");

        return switch (scanner.nextInt()) {
            case 1 -> GameAction.ROCK;
            case 2 -> GameAction.PAPER;
            case 3 -> GameAction.SCISSORS;
            default -> throw new InvalidParameterException();
        };
    }

    @Override
    public String playerWin() {
        return "Human wins";
    }
}
