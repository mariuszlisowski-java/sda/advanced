package paperscissorsstone.player;

import paperscissorsstone.enums.GameAction;
import paperscissorsstone.enums.GameResult;
import paperscissorsstone.language.Translation;

public class Game {
    private final Player player1;
    private final Player player2;
    private final Translation translation;

    public Game(Player player1, Player player2, Translation translation) {
        this.player1 = player1;
        this.player2 = player2;
        this.translation = translation;
    }

    public void startGame() {
        GameAction action1 = player1.chooseAction();
        GameAction action2 = player2.chooseAction();
        System.out.println(translation.actionText1(action1, player1));
        System.out.println(translation.actionText2(action2, player2));

        GameResult result = checkResult(action1, action2);

        switch (result) {
            case PLAYER_1_WIN -> System.out.println(player1.playerWin());
            case PLAYER_2_WIN -> System.out.println(player2.playerWin());
            case DRAW -> System.out.println("No winners");
        }
    }

    private GameResult checkResult(GameAction action1, GameAction action2) {
        if (action1 == action2) {
            return GameResult.DRAW;
        }
        if ((action1 == GameAction.PAPER && action2 == GameAction.ROCK) ||
                (action1 == GameAction.SCISSORS && action2 == GameAction.PAPER) ||
                (action1 == GameAction.ROCK && action2 == GameAction.SCISSORS))
        {
            return GameResult.PLAYER_1_WIN;
        }

        return GameResult.PLAYER_2_WIN;
    }
}
