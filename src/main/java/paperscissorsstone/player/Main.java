package paperscissorsstone.player;

import paperscissorsstone.language.EnglishTranslation;
import paperscissorsstone.language.Translation;

public class Main {
    public static void main(String[] args) {
        Translation translation = new EnglishTranslation();

        new Game(new RandomPlayer(), new ScannerPlayer(), translation).startGame();
    }

}
