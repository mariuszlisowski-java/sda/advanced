package paperscissorsstone.player;

import paperscissorsstone.enums.GameAction;

import java.util.Random;

public class RandomPlayer implements Player {
    @Override
    public GameAction chooseAction() {
        return switch (new Random().nextInt(3)) {
            case 0 -> GameAction.PAPER;
            case 1 -> GameAction.ROCK;
            case 2 -> GameAction.SCISSORS;
            default -> throw new IllegalStateException("Unexpected value");
        };
    }

    @Override
    public String playerWin() {
        return "Computer wins";
    }

}
