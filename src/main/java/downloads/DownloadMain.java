package downloads;

public class DownloadMain {

    public static void main(String[] args) {
        Observer observer = new Observer();
        observer.requestDownload(1);
    }

}
