package downloads;

import java.util.Random;

public class DownloadProcess implements Runnable {
    private final int processId;
    private static int counter = 1;
    private final Progress listener;
    private Random random = new Random();

    public DownloadProcess(Progress listener) {
        this.listener = listener;
        processId = counter++;
    }

    public int getProcessId() {
        return processId;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(random.nextInt(200));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int progress = (i + 1) * 10; // progress simulator
            listener.onProgress(processId, progress);
            System.out.println(name + " progress: " + progress + "%");
        }
        System.out.println(name + " Complete!");
        listener.onComplete();
    }

    // nested interface
    public interface Progress {
        void onProgress(int key, int percent);
        void onComplete();
//        void onError();
    }
}
